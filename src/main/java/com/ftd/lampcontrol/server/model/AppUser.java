package com.ftd.lampcontrol.server.model;

import java.util.List;
import java.util.Set;

public class AppUser extends BaseEntity<Long>{

    private String username;
    private String account;
    private String password;
    private String userage;
    private String useremail;
    private String userpic;
    private String devices;
    private String shopthing;
    private String phonenumber;

    private List<Group> groups;

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserage() {
        return userage;
    }

    public void setUserage(String userage) {
        this.userage = userage;
    }


    public String getUseremail() {
        return useremail;
    }

    public void setUseremail(String useremail) {
        this.useremail = useremail;
    }

    public String getUserpic() {
        return userpic;
    }

    public void setUserpic(String userpic) {
        this.userpic = userpic;
    }

    public String getDevices() {
        return devices;
    }

    public void setDevices(String devices) {
        this.devices = devices;
    }

    public String getShopthing() {
        return shopthing;
    }

    public void setShopthing(String shopthing) {
        this.shopthing = shopthing;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    @Override
    public String toString() {
        return "AppUser{" +
                "username='" + username + '\'' +
                ", account='" + account + '\'' +
                ", password='" + password + '\'' +
                ", userage='" + userage + '\'' +
                ", useremail='" + useremail + '\'' +
                ", userpic='" + userpic + '\'' +
                ", devices='" + devices + '\'' +
                ", shopthing='" + shopthing + '\'' +
                ", phonenumber='" + phonenumber + '\'' +
                '}';
    }
}
