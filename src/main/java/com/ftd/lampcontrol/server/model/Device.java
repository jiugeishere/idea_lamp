package com.ftd.lampcontrol.server.model;

public class Device extends BaseEntity<Long>{
    private String devicename;
    private String devicestatus;
    private String devicebackname;
    private String devicephoto;
    private String deviceType;
    private Long groupid;
    private String groupname;

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDevicename() {
        return devicename;
    }

    public void setDevicename(String devicename) {
        this.devicename = devicename;
    }

    public String getDevicestatus() {
        return devicestatus;
    }

    public void setDevicestatus(String devicestatus) {
        this.devicestatus = devicestatus;
    }

    public String getDevicebackname() {
        return devicebackname;
    }

    public void setDevicebackname(String devicebackname) {
        this.devicebackname = devicebackname;
    }

    public String getDevicephoto() {
        return devicephoto;
    }

    public void setDevicephoto(String devicephoto) {
        this.devicephoto = devicephoto;
    }

    public Long getGroupid() {
        return groupid;
    }

    public void setGroupid(Long groupid) {
        this.groupid = groupid;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }
}
