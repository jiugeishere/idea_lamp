package com.ftd.lampcontrol.server.model;

import java.util.List;
import java.util.Set;

public class Group extends BaseEntity<Long>{
    private String groupname;
    private String groupbackname;
    private List<Device> deviceSet;
    private Long userid;
    private int devicenum;

    public int getDevicenum() {
        return devicenum;
    }

    public void setDevicenum(int devicenum) {
        this.devicenum = devicenum;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public String getGroupbackname() {
        return groupbackname;
    }

    public void setGroupbackname(String groupbackname) {
        this.groupbackname = groupbackname;
    }

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    public List<Device> getDeviceSet() {
        return deviceSet;
    }

    public void setDeviceSet(List<Device> deviceSet) {
        this.deviceSet = deviceSet;
    }

    @Override
    public String toString() {
        return "Group{" +
                "groupname='" + groupname + '\'' +
                ", groupbackname='" + groupbackname + '\'' +
                ", deviceSet=" + deviceSet +
                ", userid=" + userid +
                '}';
    }
}
