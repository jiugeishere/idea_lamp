package com.ftd.lampcontrol.server.model;


public class Result<T>  {
    String code;
    String msg;
    T data;
    String num;
    String total;
    public Result(String num, String message) {
        this.num = num;
        this.msg = message;
    }
    public Result() {
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String status) {
        this.code = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}

