package com.ftd.lampcontrol.server.dto;

import com.ftd.lampcontrol.server.model.AppUser;

import java.util.List;

public class AppUserDto extends AppUser {


	private List<Long> roleIds;

	public List<Long> getRoleIds() {
		return roleIds;
	}

	public void setRoleIds(List<Long> roleIds) {
		this.roleIds = roleIds;
	}

}
