package com.ftd.lampcontrol.server.dao;

import com.ftd.lampcontrol.server.model.AppUser;
import com.ftd.lampcontrol.server.model.Device;
import com.ftd.lampcontrol.server.model.Group;
import org.apache.ibatis.annotations.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Mapper
public interface AppUserDao {

    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Insert("insert into app_user(username, account, password, userpic, userage, useremail, devices, shopthing, phonenumber, createTime, updateTime) values(#{username}, #{account}, #{password}, #{userpic}, #{userage}, #{useremail}, #{devices}, #{shopthing}, #{phonenumber}, now(), now())")
    int save(AppUser user);

    @Select("select * from app_user t where t.id = #{id}")
    AppUser getById(Long id);

    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Insert("insert into user_group(groupname,groupbackname, userid, createTime, updateTime) values(#{groupname}, #{groupbackname}, #{userid}, now(), now())")
    int saveGroup(Group group);

    @Select("select * from user_group t where t.userid = #{id}")
    List<Group> getGroupById(Long id);

    @Select("select * from user_group t where t.id = #{id}")
    Group getOneGroupById(Long id);

    @Delete("delete from user_group where id = #{id}")
    int deleteGroup(Long id);

    @Update("update user_group t set t.groupname = #{groupname},t.groupbackname=#{groupbackname},t.updateTime=#{updateTime} where t.id = #{id}")
    int upDateGroup(@Param("id") Long id, @Param("groupname") String groupname, @Param("groupbackname") String groupbackname, Date updateTime);

    @Select("select * from app_user t where t.username = #{username}")
    AppUser getUser(String username);

    @Select("select * from app_user t where t.account = #{account}")
    AppUser getUserByAccount(String account);
    @Delete("delete from app_user where id = #{id}")
    int delete(Long id);


    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Insert("insert into group_device(devicename,devicestatus,devicebackname,devicephoto,groupname,groupid,deviceType, createTime, updateTime) values(#{devicename}, #{devicestatus}, #{devicebackname},#{devicephoto},#{groupname},#{groupid},#{deviceType},now(), now())")
    int saveDevice(Device device);

    int updateDevice(Device device);

    @Delete("delete from group_device where id = #{id}")
    int deleteDevice(Long id);

    @Select("select * from group_device t where t.groupid = #{id}")
    List<Device> getDeviceById(Long id);

    @Select("select * from group_device t where t.id = #{id}")
    Device getOneDeviceById(Long id);

    @Select("select * from app_user t where t.account = #{account}")
    AppUser checkAccount(String account);

    @Update("update app_user t set t.password = #{password} where t.id = #{id}")
    int changePassword(@Param("id") Long id, @Param("password") String password);

    Integer count(@Param("params") Map<String, Object> params);

    List<AppUser> list(@Param("params") Map<String, Object> params, @Param("offset") Integer offset,
                       @Param("limit") Integer limit);

    Integer countGroup(@Param("params") Map<String, Object> params,Long id);

    List<Group> listGroup(@Param("id") Long id,@Param("params") Map<String, Object> params, @Param("offset") Integer offset,
                       @Param("limit") Integer limit);


    Integer countDevice(@Param("params") Map<String, Object> params,Long id);

    List<Device> listDevice(@Param("id") Long id,@Param("params") Map<String, Object> params, @Param("offset") Integer offset,
                          @Param("limit") Integer limit);


    @Delete("delete from app_role_user where userId = #{userId}")
    void deleteUserRole(Long userId);

    void saveUserRoles(@Param("userId") Long userId, @Param("roleIds") List<Long> roleIds);

    int update(AppUser user);
}
