package com.ftd.lampcontrol.server.service;

import com.ftd.lampcontrol.server.dto.AppUserDto;
import com.ftd.lampcontrol.server.model.AppUser;
import com.ftd.lampcontrol.server.model.Group;

import java.util.List;

public interface AppUserService {

	AppUser saveAppUser(AppUserDto userDto);

	AppUser updateAppUser(AppUserDto userDto);

	AppUser getAppUser(String account);

	void changePassword(String username, String oldPassword, String newPassword);

	List<Group> getGroup(Long userid);

	int saveGroup(Group group);








}
