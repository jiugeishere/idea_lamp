package com.ftd.lampcontrol.server.service;

import com.ftd.lampcontrol.server.dto.UserDto;
import com.ftd.lampcontrol.server.model.SysUser;

public interface UserService {

	SysUser saveUser(UserDto userDto);

	SysUser updateUser(UserDto userDto);

	SysUser getUser(String username);

	void changePassword(String username, String oldPassword, String newPassword);

}
