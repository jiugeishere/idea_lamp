package com.ftd.lampcontrol.server.service;

import com.ftd.lampcontrol.server.dto.RoleDto;

public interface RoleService {

	void saveRole(RoleDto roleDto);

	void deleteRole(Long id);
}
