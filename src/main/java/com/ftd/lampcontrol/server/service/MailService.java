package com.ftd.lampcontrol.server.service;

import java.util.List;

import com.ftd.lampcontrol.server.model.Mail;

public interface MailService {

	void save(Mail mail, List<String> toUser);
}
